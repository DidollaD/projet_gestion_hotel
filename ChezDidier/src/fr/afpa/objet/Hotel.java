package fr.afpa.objet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

public class Hotel {
	private Chambre[] listeChb; // la liste des chambres de l'hotel
	private int nbrChb; // Nombre de chambre dans l'hotel
	private Utilisateurs[] listeUser; // la liste des utilisateurs de l'application
	private int nbrUser; // Nombre de comptes utilisateurs de l'application
	private double cA; // le chriffre d'affaire de hotel a l'instant t

	public Hotel() throws IOException, InterruptedException, AddressException, MessagingException {
		Scanner sc = new Scanner(System.in);
		listeChb = new Chambre[70];
		CreerListeChb(".\\Entrer\\ListeChambres_V3.csv");
		listeUser = new Utilisateurs[30];
		CreerListeUser(".\\Entrer\\ListeEmpl.csv", nbrChb);
		cA = 2;
		int valide = -1;
		char choix = '0';
		while (choix != 'q') {
			while (valide == -1) {
				valide = EcranAuthentification(sc);
			}
			if (valide == -2) {
				while (choix != 'q') {
					AfficherMenu();
					choix = ChoixUser(sc);
				}
			} else {
				AfficherReservation(valide);
				Thread.currentThread();
				Thread.sleep(20000);
				System.out.println("\n\n\n\n\n\n\n\n");
				valide = -1;
			}
		}
		sc.close();
	}

	private int EcranAuthentification(Scanner sc) { // fonction qui demande a l'utilisateur retourne -2 si un employ� se
													// connecte, retourne la place dans le tableau des utilisateur du
													// client qui se connect
		// d'entre so login et son mot de passe
		System.out.println("Entrer votre login");
		String login = sc.nextLine(); // login entrer par l'utilisateur
		System.out.println("Entrer votre mot de passe");
		String mdp = sc.nextLine(); // mot de passe entrer par l'utilisateur
		int valideLogin = compareLogin(login);
		if (valideLogin >= 0 && mdp.equals(listeUser[valideLogin].getMdp())) {
			System.out.println("\n\n\n\n\n\n\n\n");
			if (listeUser[valideLogin].isEmploye() == true) { // retourne -2 si un employ� se connecte, retourne la
																// place dans le tableau des utilisateur du client qui
																// se connect
				return -2;
			} else {
				if (listeUser[valideLogin].getNbrResa() > 0) { // retourne la place dans le tableau des utilisateur du
																// client qui se connect si le client a une reservation
					return valideLogin;
				} else {
					System.out.println("Ce client n'a pas de reservation");
				}
			}
		} else {
			System.out.println("Saisie invalide");
		}
		return -1; // retourne -1 si la saisie est invalide
	}

	private void AfficherMenu() {// fonction qui affiche les differente fonctionalite de application et la lettre
									// a saisir pour les utilis�
		System.out.println(
				"------------------------------- MENU HOTEL CDA JAVA ------------------------------------------------\r\n"
						+ "A- Afficher l’état de l’hôtel\r\n" + "B- Afficher le nombre de chambres réservées\r\n"
						+ "C- Afficher le nombre de chambres libres\r\n"
						+ "D- Afficher le num�ro de la premi�re chambre vide\r\n"
						+ "E- Afficher le num�ro de la derni�re chambre vide\r\n" + "F- R�server une chambre\r\n"
						+ "G- Lib�rer une chambre\r\n" + "H- Modifier une r�servation\r\n"
						+ "I- Annuler une r�servation\r\n" + "Q- Quitter\r\n"
						+ "---------------------------------------------------------------------------------------------------------------------\r\n\n");
		System.out.print("Votre choix : ");
	}

	private void AfficherReservation(int valideLogin) { // Affiche les reservation d'un client
		System.out.println("Client n� : " + listeUser[valideLogin].getLogin() + "    " + listeUser[valideLogin].getNom()
				+ " " + listeUser[valideLogin].getPrenom());
		for (int i = 0; i < listeUser[valideLogin].getNbrResa(); i++) {
			for (int j = 0; j < nbrChb; j++) {
				if (listeChb[j].isOccupe() == true) {
					for (int k = 0; k < listeChb[j].getNbrResa(); k++) {
						if (listeChb[j].getListeReservations()[k].getUser().getLogin()
								.equals(listeUser[valideLogin].getLogin())) {
							System.out.println("    Chambre " + listeChb[j].getNmrChb() + " du "
									+ listeChb[j].getListeReservations()[k].getDateDebut() + " au "
									+ listeChb[j].getListeReservations()[k].getDateFin());
							break;
						}
					}
				}

			}
		}
	}

	private char ChoixUser(Scanner sc) throws IOException, AddressException, MessagingException { // Fonction qui appel
																									// les fonction
																									// selon la lettre
			                 																		// entrer par
																									// l'utilisateur
		char choix = '0';																						
		try {
			choix = sc.nextLine().toLowerCase().charAt(0);
		} catch (Exception e) {
			
		}																						
		 
		switch (choix) {
		case 'a':
			AfficherEtatHotel();
			break;
		case 'b':
			AfficherChambresReservees();
			break;
		case 'c':
			AfficherChambresLibres();
			break;
		case 'd':
			AfficherPremiereChambreVide();
			break;
		case 'e':
			AfficherDerniereChambreVide();
			break;
		case 'f':
			ReserverChambre(sc);
			break;
		case 'g':
			LibererChambre(sc);
			break;
		case 'h':
			ModifierReservation(sc);
			break;
		case 'i':
			AnnulerReservation(sc);
			break;
		case 'q':
			CsvGenerateur(".\\Entrer\\ListeEmpl.csv");
			System.out.println("Au revoir");
			break;
		default:
			System.out.println("Saisie invalide");
			break;
		}
		return choix;
	}

	private void AfficherEtatHotel() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		for (int i = 0; i < nbrChb; i++) {
			if (listeChb[i].isOccupe() == true) {
				System.out.println("La chambre " + listeChb[i].getNmrChb() + " est occupe : ");
				for (int j = 0; j < listeChb[i].getNbrResa(); j++) {
					System.out.print("     Du " + dtf.format(listeChb[i].getListeReservations()[j].getDateDebut())
							+ " au " + dtf.format(listeChb[i].getListeReservations()[j].getDateFin()) + " par Mr/M "
							+ listeChb[i].getListeReservations()[j].getUser().getPrenom() + " "
							+ listeChb[i].getListeReservations()[j].getUser().getNom());
				}
				System.out.println();
			}
		}
	}

	private void AfficherChambresReservees() {
		int occupe = 0;
		for (int i = 0; i < nbrChb; i++) {
			if (listeChb[i].isOccupe() == true) {
				occupe++;
			}
		}
		System.out.println("il y a " + occupe + " chambre réserver");
	}

	private void AfficherChambresLibres() {
		int libre = 0;
		for (int i = 0; i < nbrChb; i++) {
			if (listeChb[i].isOccupe() == false) {
				libre++;
			}
		}
		System.out.println("il y a " + libre + "chambre libre");
	}

	private void AfficherPremiereChambreVide() {
		for (int i = 0; i < nbrChb; i++) {
			if (listeChb[i].isOccupe() == false) {
				System.out.println("la premiere chambre vide est la chambre " + listeChb[i].getNmrChb());
				break;
			}
		}
	}

	private void AfficherDerniereChambreVide() {
		for (int i = nbrChb - 1; i >= 0; i--) {
			if (listeChb[i].isOccupe() == false) {
				System.out.println("la dernier chambre vide est la chambre " + listeChb[i].getNmrChb());
				break;
			}
		}
	}

	private void ReserverChambre(Scanner sc) throws IOException, AddressException, MessagingException {
		boolean comfirmAdmin = ConfirmeAdmin(sc);
		if (comfirmAdmin == true) {
			int indexClient = ChoisirUser(sc);
			int indexChambre = ChoisirChambre(sc);

			LocalDate dateD = null;
			while (dateD == null) {
				System.out.println("entrer la date de debut de réservation dd/mm/yyyy");
				dateD = ConfirmDate(sc);
				for (int i = 0; i < listeChb[indexChambre].getNbrResa() && dateD != null; i++) {
					if (dateD.isBefore(listeChb[indexChambre].getListeReservations()[i].getDateFin())
							&& dateD.isAfter(listeChb[indexChambre].getListeReservations()[i].getDateDebut())) {
						System.out.println("cette date enpiete sur une autre reservation");
						dateD = null;
						break;
					}
				}
			}
			LocalDate dateF = null;
			while (dateF == null) {
				System.out.println("entrer la date de fin de réservation dd/mm/yyyy");
				dateF = ConfirmDate(sc);
				if (dateF != null && dateF.isBefore(dateD)) {
					System.out.println("Date de fin anterieure a la date de debut");
					dateF = null;
				}
				for (int i = 0; i < listeChb[indexChambre].getNbrResa() && dateF != null; i++) {
					if (dateF.isBefore(listeChb[indexChambre].getListeReservations()[i].getDateFin())
							&& dateF.isAfter(listeChb[indexChambre].getListeReservations()[i].getDateDebut())) {
						System.out.println("cette date enpiete sur une autre reservation");
						dateF = null;
						break;
					}
				}
			}
			long nbrJours = (int) ChronoUnit.DAYS.between(dateD, dateF);
			String nmrCb = DemandeNmrCB(sc);
			listeChb[indexChambre].getListeReservations()[listeChb[indexChambre].getNbrResa()] = new Reservation(dateD,
					dateF, listeUser[indexClient]);
			listeChb[indexChambre].setNbrResa(listeChb[indexChambre].getNbrResa() + 1);
			listeChb[indexChambre].setOccupe(true);
			listeUser[indexClient].setNbrResa(listeUser[indexClient].getNbrResa() + 1);
			String chemin = PdfGenerateur(indexChambre, "Payment de la réservation", nmrCb, nbrJours);
//			try {
//				EnvoiMessage(chemin, indexClient , "Payment de la réservation");
//			} catch (Exception e) {
			// System.out.println("message non envoyé");
//			}

		}
	}

	private int[] LibererChambre(Scanner sc) throws IOException {
		boolean comfirmAdmin = ConfirmeAdmin(sc);
		int[] info = new int[3];
		if (comfirmAdmin == true) {
			System.out.println("Quel chambre voulez-vous Liberer");
			String ChbALiberer = sc.nextLine();
			for (int i = 0; i < nbrChb; i++) {
				if (listeChb[i].getNmrChb().equals(ChbALiberer)) {
					if (listeChb[i].isOccupe() == false) {
						System.out.println("Cette chambre est déjà libre");
					} else {
						for (int j = 0; j < listeChb[i].getNbrResa(); j++) {
							System.out.println(listeChb[i].getListeReservations()[j].getUser().getLogin());
						}
						System.out.println("De quel client voulez-vous Liberer la chambre");
						String CltLiberer = sc.nextLine();
						for (int j = 0; j < listeChb[i].getNbrResa(); j++) {
							if (listeChb[i].getListeReservations()[j].getUser().getLogin().equals(CltLiberer)) {
								listeChb[i].getListeReservations()[j].getUser()
										.setNbrResa(listeChb[i].getListeReservations()[j].getUser().getNbrResa() - 1);
								for (int k = 0; k < nbrUser; k++) {
									if (listeChb[i].getListeReservations()[j].getUser().getLogin()
											.equals(listeUser[k].getLogin())) {
										info[0] = k;
										info[1] = i;
										// ---------
										Period duree = Period.between(
												listeChb[i].getListeReservations()[j].getDateDebut(),
												listeChb[i].getListeReservations()[j].getDateFin());
										info[2] = duree.getDays();
										// ---------
										break;
									}
								}
								listeChb[i].SupprReservations(j);
								System.out.println("Reservation supprimer");
								break;
							}
						}
					}
					break;
				}
			}
		}
		return info;
	}

	private void ModifierReservation(Scanner sc) throws IOException {
		boolean comfirmAdmin = ConfirmeAdmin(sc);
		if (comfirmAdmin == true) {
			for (int i = 0; i < nbrChb; i++) {
				if (listeChb[i].isOccupe() == true) {
					System.out.println(listeChb[i].getNmrChb());
				}
			}
			System.out.println("De quel chambre voulez-vous modifier la reservation ?");
			String ChbALiberer = sc.nextLine();
			for (int i = 0; i < nbrChb; i++) {
				if (listeChb[i].getNmrChb().equals(ChbALiberer)) {
					if (listeChb[i].isOccupe() == false) {
						System.out.println("Cette chambre est déjà libre");
					} else {
						for (int j = 0; j < listeChb[i].getNbrResa(); j++) {
							System.out.println(listeChb[i].getListeReservations()[j].getUser().getLogin());
						}

						System.out.println("De quel client voulez-vous modifier la reservation ?");
						String CltLiberer = sc.nextLine();
						for (int j = 0; j < listeChb[i].getNbrResa(); j++) {
							if (listeChb[i].getListeReservations()[j].getUser().getLogin().equals(CltLiberer)) {
								long nbrJours = (int) ChronoUnit.DAYS.between(
										listeChb[i].getListeReservations()[j].getDateDebut(),
										listeChb[i].getListeReservations()[j].getDateFin());
								LocalDate dateD = null;
								listeChb[i].getListeReservations()[j].setDateDebut(null);
								while (dateD == null) {
									System.out.println("entrer la date de debut de réservation dd/mm/yyyy");
									dateD = ConfirmDate(sc);
									for (int k = 0; k < listeChb[i].getNbrResa() && dateD != null; k++) {
										if (listeChb[i].getListeReservations()[k].getDateDebut() != null
												&& dateD.isBefore(listeChb[i].getListeReservations()[k].getDateFin())
												&& dateD.isAfter(
														listeChb[i].getListeReservations()[k].getDateDebut())) {
											System.out.println("cette date enpiete sur une autre reservation");
											dateD = null;
											break;
										}
									}
								}
								listeChb[i].getListeReservations()[j].setDateDebut(dateD);
								LocalDate dateF = null;
								listeChb[i].getListeReservations()[j].setDateFin(null);
								while (dateF == null) {
									System.out.println("entrer la date de fin de réservation dd/mm/yyyy");
									dateF = ConfirmDate(sc);
									if (dateF != null && dateF.isBefore(dateD)) {
										System.out.println("Date de fin anterieure a la date de debut");
										dateF = null;
									}
									for (int k = 0; k < listeChb[i].getNbrResa() && dateF != null; k++) {
										if (listeChb[i].getListeReservations()[k].getDateFin() != null
												&& dateF.isBefore(listeChb[i].getListeReservations()[k].getDateFin())
												&& dateF.isAfter(
														listeChb[i].getListeReservations()[k].getDateDebut())) {
											System.out.println("cette date enpiete sur une autre reservation");
											dateF = null;
											break;
										}
									}
								}
								listeChb[i].getListeReservations()[j].setDateFin(dateF);
								long nbrJoursModif = (int) ChronoUnit.DAYS.between(dateD, dateF);
								long diffJours = nbrJoursModif - nbrJours;
								System.out.println("La reservation dure " + diffJours
										+ " jour(s) de difference avec l'ancienne date");
								String nmrCb = DemandeNmrCB(sc);
								if (diffJours >= 0) {
									String chemin = PdfGenerateur(i, "Paiement supplémentaire", nmrCb, nbrJours);
								} else if (diffJours < 0) {
									String chemin = PdfGenerateur(i, "Remboursement", nmrCb, (diffJours * -2 / 2));
								}
//							try {
//								EnvoiMessage(chemin, indexClient , "Payment de la réservation");
//							} catch (Exception e) {
								System.out.println("message non envoyé");
//							}
								break;
							}
						}
					}
				}
			}
		}
	}

	private void AnnulerReservation(Scanner sc) throws IOException {
		int[] info = LibererChambre(sc);
		String nmrCb = DemandeNmrCB(sc);
		String chemin = PdfGenerateur(info[1], "Reboursement", nmrCb, info[2]);
//		try {
//			EnvoiMessage(chemin, info[0] , "Reboursement de la réservation");
//		} catch (Exception e) {
		System.out.println("message non envoyé");
//		}
	}

	private int ChoisirUser(Scanner sc) {
		int indexClient = -1;
		boolean flag = false;
		char choix = '0';
		while (flag == false) {
			System.out.println("Client éxistant ou nouveau client ? E/N");
			choix = sc.nextLine().toLowerCase().charAt(0);
			String login = "";
			if (choix == 'n') {
				while (flag == false) {
					flag = true;
					System.out.println("Entrez un login : ");
					login = sc.nextLine();
					if (login.length() != 10) {
						System.out.println("saisie invalide");
					} else {
						for (int i = 0; i < login.length(); i++) {
							if ((int) login.charAt(i) > 57 || (int) login.charAt(i) < 48) {
								flag = false;
								System.out.println("saisie invalide");
								break;
							}
						}
						if (flag == true) {
							for (int i = 0; i < nbrUser; i++) {
								if (login.equalsIgnoreCase(listeUser[i].getLogin())) {
									flag = false;
									break;
								}
							}
							if (flag == false) {
								System.out.println("Login indisponible");
							} else {
								System.out.println("Veuillez entrer un mot de passe");
								String mdp = sc.nextLine();
								System.out.println("Veuillez entrer un nom");
								String nom = sc.nextLine();
								System.out.println("Veuillez entrer un prenom");
								String prenom = sc.nextLine();
								String addrMail = null;
								while (addrMail == null) {
									System.out.println("Veuillez entrer une addresse email");
									addrMail = VerifierAddrMail(sc);
								}
								listeUser[nbrUser] = new Utilisateurs(nom, prenom, login, mdp, addrMail, false);
								indexClient = nbrUser;
								nbrUser++;
								return indexClient;
							}
						}
					}
				}

			} else if (choix == 'e') {
				while (login.length() != 10) {
					System.out.println("Veuillez entrer le login de l'utilisateur : ");
					login = sc.nextLine();
				}
				for (int i = 0; i < nbrUser; i++) {
					if (login.equalsIgnoreCase(listeUser[i].getLogin())) {
						if (listeUser[i].getNbrResa() == 5) {
							System.out.println("Le client a déjà 5 réservation");
						} else {
							indexClient = i;
							return indexClient;
						}

					}
				}
			} else {
				System.out.println("Saisie invalide");
			}
		}
		return indexClient;
	}

	private String VerifierAddrMail(Scanner sc) {
		String addrMail = sc.nextLine();
		String[] DebutAddr = addrMail.split("@");
		if (DebutAddr.length == 2 && DebutAddr[0].length() > 2) {
			if ((int) DebutAddr[0].charAt(DebutAddr[0].length() - 1) == 46) {
				System.out.println("Saisie invalide");
				return null;
			}
			for (int i = 0; i < DebutAddr[0].length() - 1; i++) {
				if ((int) DebutAddr[0].charAt(i) == 46 && (int) DebutAddr[0].charAt(i + 1) == 46) {
					System.out.println("Saisie invalide");
					return null;
				}
			}
			for (int i = 0; i < DebutAddr[0].length(); i++) {
				if ((int) DebutAddr[0].charAt(i) < 43 || (int) DebutAddr[0].charAt(i) == 44
						|| (int) DebutAddr[0].charAt(i) == 47
						|| ((int) DebutAddr[0].charAt(i) > 57 && (int) DebutAddr[0].charAt(i) < 65)
						|| ((int) DebutAddr[0].charAt(i) > 90 && (int) DebutAddr[0].charAt(i) < 95)
						|| (int) DebutAddr[0].charAt(i) == 96 || (int) DebutAddr[0].charAt(i) > 122) {
					System.out.println("Saisie invalide");
					return null;
				}
			}

			String[] FinAddr = DebutAddr[1].split("\\.");
			if (FinAddr.length == 2 && FinAddr[1].length() > 0 && FinAddr[0].length() > 0) {
				for (int i = 0; i < FinAddr.length; i++) {
					for (int j = 0; j < FinAddr[i].length(); j++) {
						if (FinAddr[i].charAt(j) < 65 || (FinAddr[i].charAt(j) > 90 && FinAddr[i].charAt(j) < 97)
								|| FinAddr[i].charAt(j) > 122) {
							System.out.println("Saisie invalide");
							return null;
						}
					}
				}
			} else {
				System.out.println("Saisie invalide");
				return null;
			}
		} else {
			System.out.println("Saisie invalide");
			return null;
		}
		return addrMail;
	}

	private int ChoisirChambre(Scanner sc) {
		int indexChambre = -1;
		while (indexChambre == -1) {
			System.out.println("Veuillez entrer le type de chambre desiree : \n" + "1-Chambre Vue Piscine\n"
					+ "2-Chambre Vue Jardin\n" + "3-Chambre Vue Océan\n" + "4-Chambre vue imprenable sur l'océan\n"
					+ "5-Suite CDA\n" + "6-Suite Executive\n" + "7-Suite Ambassadeur\n" + "8-Suite Royale");
			char choixType = '0';
			String type = "";
			while (choixType == '0') {
				choixType = sc.nextLine().charAt(0);
				switch (choixType) {
				case '1':
					type = "Chambre Vue Piscine";
					break;
				case '2':
					type = "Chambre Vue Jardin";
					break;
				case '3':
					type = "Chambre Vue Océan";
					break;
				case '4':
					type = "Chambre vue imprenable sur l'océan";
					break;
				case '5':
					type = "Suite CDA";
					break;
				case '6':
					type = "Suite Executive";
					break;
				case '7':
					type = "Suite Ambassadeur";
					break;
				case '8':
					type = "Suite Royale";
					break;
				default:
					System.out.println("Choix invalide");
					choixType = '0';
					break;
				}
			}
			String[] chambreTypeDesire = new String[12];
			int nbrChambreTypeDesire = 0;
			for (int i = 0; i < nbrChb; i++) {
				if (listeChb[i].getTypeChb().equals(type)) {
					chambreTypeDesire[nbrChambreTypeDesire] = listeChb[i].getNmrChb();
					nbrChambreTypeDesire++;
					System.out.println(listeChb[i].getNmrChb());
				}
			}
			if (nbrChambreTypeDesire == 0) {
				System.out.println("aucun chambre de se type n'est disponible");
			} else {
				System.out.println("entrer le numero de la chambre désiree");
				String nmrChambreDesire = sc.nextLine();
				for (int i = 0; i < nbrChambreTypeDesire; i++) {
					if (chambreTypeDesire[i].equals(nmrChambreDesire)) {
						for (int j = 0; j < nbrChb; j++) {
							if (nmrChambreDesire.equals(listeChb[j].getNmrChb())) {
								indexChambre = j;
							}
						}
					}
				}
			}
		}
		return indexChambre;
	}

	private int compareLogin(String login) {
		for (int i = 0; i < nbrUser; i++) {
			if (login.equalsIgnoreCase(listeUser[i].getLogin())) {
				return i;
			}
		}
		return -1;
	}

	private boolean ConfirmeAdmin(Scanner sc) throws IOException {
		BufferedReader br = chercherFichier(".\\Entrer\\MdpAdmin.txt");
		String mdpAdmin = br.readLine();
		System.out.println("entrer le mot de passe administrateur : ");
		String mdpAdminEntre = sc.nextLine();
		if (mdpAdmin.equals(mdpAdminEntre))
			return true;
		return false;
	}

	private LocalDate ConfirmDate(Scanner sc) {
		try {
			String dateIn = sc.nextLine();
			String[] divisionDate = dateIn.split("/");
			LocalDate date = LocalDate.parse(divisionDate[2] + "-" + divisionDate[1] + "-" + divisionDate[0]);
			return date;
		} catch (Exception e) {
			System.out.println("Format de date invalide");
		}
		return null;
	}

	private BufferedReader chercherFichier(String file) {
		FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			return br;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String DemandeNmrCB(Scanner sc) {
		System.out.println("Entrer le numéro de la carte du paiement");
		boolean flag = false;
		String nmrCb = "";
		while (flag == false) {
			nmrCb = sc.nextLine();
			if (nmrCb.length() == 16) {
				flag = true;
				break;
			}
		}
		return nmrCb;
	}

	private void EnvoiMessage(String chemin, int indexClient, String texte)
			throws AddressException, MessagingException, IOException {
		String user = "chezdidiern1@gmail.com";
		String pass = "didiern1";
		String serveur = "smtp.gmail.com";
		Properties props = System.getProperties();
		props.put("mail.smtp.host", serveur);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", 587);
		props.put("mail.smtp.auth", "true");
		Session session = Session.getInstance(props);
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(user));
		InternetAddress[] internetAddresses = new InternetAddress[1];
		internetAddresses[0] = new InternetAddress(listeUser[indexClient].getAddrMail());
		message.setRecipients(Message.RecipientType.TO, internetAddresses);
		message.setSubject(texte);
		MimeBodyPart body = new MimeBodyPart();
		body.setText(texte);
		MimeBodyPart attachMent = new MimeBodyPart();
		attachMent.attachFile(chemin);
		Multipart multi = new MimeMultipart();
		multi.addBodyPart(body);
		multi.addBodyPart(attachMent);
		message.setContent(multi);
		Transport transport = session.getTransport("smtp");
		transport.connect(serveur, user, pass);
		transport.sendMessage(message, internetAddresses);
		transport.close();
	}

	private String PdfGenerateur(int indexChb, String transaction, String nmrCb, long nbrJours)
			throws FileNotFoundException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("_dd_MM_yyyy_HH_mm_ss");
		LocalDateTime now = LocalDateTime.now();
		String chemin = ".\\Entrer\\pdfgeneration\\Reservation" + dtf.format(now) + ".pdf";
		PdfWriter writer = new PdfWriter(chemin);
		PdfDocument pdf = new PdfDocument(writer);
		Document document = new Document(pdf, PageSize.A6);
		dtf = DateTimeFormatter.ofPattern("dd MM yyyy à HH:mm");
		Paragraph p = new Paragraph().setTextAlignment(TextAlignment.CENTER);
		p.add("-----------------Chez Didier-----------------\n");
		p.add("enregistré le " + dtf.format(now) + "\n");
		p.add(transaction + "\n");
		p.add((listeChb[indexChb].getTarifChb() * nbrJours) + "euros\n");
		p.add("Carte utilisé : " + nmrCb);
		document.add(p);
		document.close();
		return chemin;
	}

	private void CsvGenerateur(String file) throws IOException {
		FileWriter writer = new FileWriter(file);
		BufferedWriter document = new BufferedWriter(writer);
		document.write("nom;prenom;login;mdp;empl;nbrReserve;reserve");
		document.newLine();
		for (int i = 0; i < nbrUser; i++) {
			document.write(listeUser[i].getNom() + ";" + listeUser[i].getPrenom() + ";" + listeUser[i].getLogin() + ";"
					+ listeUser[i].getMdp() + ";");
			if (listeUser[i].isEmploye() == true) {
				document.write("oui");
			} else {
				document.write("non;" + listeUser[i].getAddrMail() + ";" + listeUser[i].getNbrResa() + ";");

				for (int j = 0; j < nbrChb; j++) {
					for (int k = 0; k < listeChb[j].getNbrResa(); k++) {
						if (listeUser[i].getLogin()
								.equals(listeChb[j].getListeReservations()[k].getUser().getLogin())) {
							document.write(listeChb[j].getListeReservations()[k].getDateDebut() + "|"
									+ listeChb[j].getListeReservations()[k].getDateFin() + "|" + listeChb[j].getNmrChb()
									+ ";");
						}
					}
				}
			}
			document.newLine();
		}
		document.close();
	}

	private void CreerListeChb(String file) {
		BufferedReader br = chercherFichier(file);
		try {
			br.readLine();
			int j = 0;
			while (br.ready()) {
				String str = br.readLine();
				String[] attributChb = str.split(";");
				String[] options = attributChb[6].split("\\|");
				for (int i = 0; i < Integer.parseInt(attributChb[5]); i++) {
					listeChb[j] = new Chambre("c" + (j + 1), attributChb[0], attributChb[1],
							Float.parseFloat(attributChb[4]), options, false);
					j++;
				}
			}
			nbrChb = j;
			br.close();
		} catch (Exception e) {
			System.out.println("Erreur liste chambre" + e);
		}
	}

	private void CreerListeUser(String file, int nbrChb) {
		BufferedReader br = chercherFichier(file);
		try {
			br.readLine();
			int i = 0;
			while (br.ready()) {
				String str = br.readLine();
				String[] attributEmpl = str.split(";");
				if (attributEmpl[4].equals("non")) {
					listeUser[i] = new Utilisateurs(attributEmpl[0], attributEmpl[1], attributEmpl[2], attributEmpl[3],
							attributEmpl[5], Integer.parseInt(attributEmpl[6]), false);

					for (int j = 0; j < Integer.parseInt(attributEmpl[6]); j++) {
						String[] attriReserve = attributEmpl[7 + j].split("\\|");
						String[] dateDebut = attriReserve[0].split("-");
						LocalDate debut = LocalDate.of(Integer.parseInt(dateDebut[0]), Integer.parseInt(dateDebut[1]),
								Integer.parseInt(dateDebut[2]));
						String[] dateFin = attriReserve[1].split("-");
						LocalDate fin = LocalDate.of(Integer.parseInt(dateFin[0]), Integer.parseInt(dateFin[1]),
								Integer.parseInt(dateFin[2]));
						int idChb = chercherChambre(attriReserve[2], listeChb, nbrChb);
						listeChb[idChb].ajoutReservations(new Reservation(debut, fin, listeUser[i]));
					}

				} else {
					listeUser[i] = new Utilisateurs(attributEmpl[0], attributEmpl[1], attributEmpl[2], attributEmpl[3],
							true);
				}
				i++;
			}
			nbrUser = i;
			br.close();
		} catch (Exception e) {
			System.out.println("Erreur liste User" + e);
		}
	}

	private int chercherChambre(String nmrChambre, Chambre[] listeChb, int nbrChb) {
		for (int i = 0; i < nbrChb; i++) {
			if (nmrChambre.equals(listeChb[i].getNmrChb())) {
				listeChb[i].setOccupe(true);
				return i;
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		String str = "Hotel [listeChb=\n";
		for (int i = 0; i < nbrChb; i++) {
			str += listeChb[i].toString() + "\n";
		}
		str += "listeUser=\n";
		for (int i = 0; i < nbrUser; i++) {
			str += listeUser[i].toString() + "\n";
		}
		str += ", cA=" + cA + "]";
		return str;
	}

	public Chambre[] getListeChb() {
		return listeChb;
	}

	public void setListeChb(Chambre[] pListeChb) {
		listeChb = pListeChb;
	}

	public int getNbrChb() {
		return nbrChb;
	}

	public void setNbrChb(int pNbrChb) {
		nbrChb = pNbrChb;
	}

	public Utilisateurs[] getListeUser() {
		return listeUser;
	}

	public void setListeUser(Utilisateurs[] pListeUser) {
		listeUser = pListeUser;
	}

	public double getcA() {
		return cA;
	}

	public void setcA(double pCA) {
		cA = pCA;
	}

	public int getNbrUser() {
		return nbrUser;
	}

	public void setNbrUser(int pNbrUser) {
		nbrUser = pNbrUser;
	}

}
