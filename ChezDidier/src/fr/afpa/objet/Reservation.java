package fr.afpa.objet;

import java.time.LocalDate;

public class Reservation {
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private Utilisateurs user;

	
	public Reservation (LocalDate pDateDebut, LocalDate pDateFin, Utilisateurs pUser) {
		dateDebut = pDateDebut;
		dateFin = pDateFin;
		user  = pUser;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate pdateDebut) {
		dateDebut = pdateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate pdateFin) {
		dateFin = pdateFin;
	}

	public Utilisateurs getUser() {
		return user;
	}

	public void setUser(Utilisateurs pUser) {
		user = pUser;
	}
}
