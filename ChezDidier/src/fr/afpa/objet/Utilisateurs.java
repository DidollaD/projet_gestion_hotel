package fr.afpa.objet;

public class Utilisateurs {
	private String nom;// le nom de l'utilisateur
	private String prenom;// le prenom de l'utilisateur
	private String login; // le login de l'utilisateur
	private String mdp; // le mot de passe de l'utilisateur
	private String addrMail; // l'addresse Email de l'utilisateur
	private boolean isEmploye;
	private int nbrResa;

	public Utilisateurs(String pNom, String pPrenom, String pLogin, String pMdp, String pAddrMail, int pNbrResa,
			boolean pIsEmploye) {
		nom = pNom;
		prenom = pPrenom;
		login = pLogin;
		mdp = pMdp;
		addrMail = pAddrMail;
		isEmploye = pIsEmploye;
		nbrResa = pNbrResa;
	}

	public Utilisateurs(String pNom, String pPrenom, String pLogin, String pMdp, String pAddrMail, boolean pIsEmploye) {
		nom = pNom;
		prenom = pPrenom;
		login = pLogin;
		mdp = pMdp;
		addrMail = pAddrMail;
		isEmploye = pIsEmploye;
		nbrResa = 0;
	}

	public Utilisateurs(String pNom, String pPrenom, String pLogin, String pMdp, boolean pIsEmploye) {
		nom = pNom;
		prenom = pPrenom;
		login = pLogin;
		mdp = pMdp;
		isEmploye = pIsEmploye;
		nbrResa = 0;
	}

	@Override
	public String toString() {
		return "Utilisateurs [nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", mdp=" + mdp + ", isEmploye="
				+ isEmploye + ", nbrResa=" + nbrResa + "]";
	}

	public void ajoutReservations() {
		nbrResa++;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String pNom) {
		nom = pNom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String pPrenom) {
		prenom = pPrenom;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String pLogin) {
		login = pLogin;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String pMdp) {
		mdp = pMdp;
	}

	public String getAddrMail() {
		return addrMail;
	}

	public void setAddrMail(String addrMail) {
		this.addrMail = addrMail;
	}

	public boolean isEmploye() {
		return isEmploye;
	}

	public void setEmploye(boolean pIsEmploye) {
		isEmploye = pIsEmploye;
	}

	public int getNbrResa() {
		return nbrResa;
	}

	public void setNbrResa(int pNbrResa) {
		nbrResa = pNbrResa;
	}

}
