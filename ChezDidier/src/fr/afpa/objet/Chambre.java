package fr.afpa.objet;

import java.util.Arrays;

public class Chambre {
	private String nmrChb; //numero de la chambre
	private String typeChb; //type d ela chambre
	private String supChb; //superficie de la chambre
	private float tarifChb; //tarif de la chambre
	private String [] optionChb; //tableau des options de la chambre
	private boolean occupe; //si la chambre est occup�e ou non
	private Reservation[] listeReservations;
	private int nbrResa;
	
	public Chambre(String pNmrChb, String pTypeChb, String pSupChb, float pTarifChb, String[] pOptionChb, boolean pOccupe) {
		nmrChb = pNmrChb;
		typeChb = pTypeChb;
		supChb = pSupChb;
		tarifChb = pTarifChb;
		optionChb = pOptionChb;
		occupe = pOccupe;
		listeReservations = new Reservation[5];
		nbrResa = 0;
	}

	@Override
	public String toString() {
		return "Chambre [nmrChb=" + nmrChb + ", typeChb=" + typeChb + ", supChb=" + supChb + ", tarifChb=" + tarifChb
				+ ", optionChb=" + Arrays.toString(optionChb) + ", occupe=" + occupe + "]";
	}

	public void ajoutReservations(Reservation reservations) {
		listeReservations[nbrResa] = reservations;
		nbrResa++;
	}

	public void SupprReservations(int idResSuppr) {
		
		for (int i = idResSuppr; i < nbrResa-1; i++) {
			Reservation temp = listeReservations[i];
			listeReservations[i] = listeReservations[i+1];
			listeReservations[i+1] = temp;
		}
		nbrResa--;
	}
	
	public String getNmrChb() {
		return nmrChb;
	}

	public void setNmrChb(String pNmrChb) {
		nmrChb = pNmrChb;
	}

	public String getTypeChb() {
		return typeChb;
	}

	public void setTypeChb(String pTypeChb) {
		typeChb = pTypeChb;
	}

	public String getSupChb() {
		return supChb;
	}

	public void setSupChb(String pSupChb) {
		supChb = pSupChb;
	}

	public float getTarifChb() {
		return tarifChb;
	}

	public void setTarifChb(float pTarifChb) {
		tarifChb = pTarifChb;
	}

	public String[] getOptionChb() {
		return optionChb;
	}

	public void setOptionChb(String[] pOptionChb) {
		optionChb = pOptionChb;
	}

	public boolean isOccupe() {
		return occupe;
	}

	public void setOccupe(boolean pOccupe) {
		occupe = pOccupe;
	}

	public Reservation[] getListeReservations() {
		return listeReservations;
	}

	public void setListeReservations(Reservation[] listeReservations) {
		this.listeReservations = listeReservations;
	}

	public int getNbrResa() {
		return nbrResa;
	}

	public void setNbrResa(int nbrResa) {
		this.nbrResa = nbrResa;
	}
	
	
	
}
